﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstanceParticle : MonoBehaviour
{ 
    [SerializeField] private GameObject _particlePrefab;
    [SerializeField] private TextWords _textWords = new TextWords(new string[] {"Thanks", "Cool", "Nice", "Ok"});
    private Button _thisBtn;
   
    private void Awake()
    {
        _thisBtn = GetComponent<Button>();
        _thisBtn.onClick.AddListener(InstaceParticle);
    }

    private void InstaceParticle()
    {
        var textParticle = Instantiate(_particlePrefab).GetComponent<TextParticle>();
        
        textParticle.transform.position = Vector3.up;
        textParticle.transform.localRotation = Quaternion.identity;

        textParticle.SetText(_textWords.Random);
        
        textParticle.Play();
        
        Destroy(textParticle.gameObject, textParticle.ParticleSystem.main.startLifetime.constantMax);
    }
}
