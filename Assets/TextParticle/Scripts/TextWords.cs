﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class TextWords
{
    public TextWords()
    {

    }

    public TextWords(string[] array)
    {
        Array = array;
    }

    public string[] Array = new string[] {""};

    public string Random
    {
        get { return Array[UnityEngine.Random.Range(0, Array.Length)]; }
    }
}