﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextCamera : MonoBehaviour
{
    public Text Text { get; private set; } 
    public RenderTexture RenderTexture { get; private set; }
    public Camera Camera { get; private set; }

    public void Init(Material material)
    {
        Camera = GetComponent<Camera>();
        Text = transform.GetComponentInChildren<Text>();

        RenderTexture = new RenderTexture(256, 256, 16, RenderTextureFormat.ARGB32);
        RenderTexture.Create();

        Camera.targetTexture = RenderTexture;

        material.mainTexture = RenderTexture;
    }

    public void SetText(string text)
    {
        if (Text != null)
            Text.text = text;
    }
}
