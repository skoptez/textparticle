﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextParticle : MonoBehaviour
{
    public TextCamera TextCamera { get; private set; }
    public ParticleSystem ParticleSystem { get; private set; }
    private Material _material;

    private void Awake()
    {
        TextCamera = transform.GetComponentInChildren<TextCamera>();
        ParticleSystem = GetComponent<ParticleSystem>();
        _material = ParticleSystem.GetComponent<Renderer>().material;

        TextCamera.Init(_material);
    }

    public void Play(string text)
    {
        TextCamera.SetText(text);
        ParticleSystem.Play();
        
      //Destroy(gameObject, _particleSystem.main.startLifetime.constantMax);
    }

    public void Play()
    {
        ParticleSystem.Play();
    }

    public void SetText(string text)
    {
        TextCamera.SetText(text);
    }
}
